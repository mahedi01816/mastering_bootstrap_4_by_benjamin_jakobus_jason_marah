$(document).ready(function () {
    $(".nav-link").on('click', function (e) {
        e.preventDefault();

        var offset = $(this.hash).offset();

        if (offset) {
            $('body').animate({
                scrollTop: offset.top
            });
        }
    });

    if ($.browser.msie && $.browser.version <= 9) {
        $('#unsupported-browser-alert').show();
    }

    $('#page-1').show();
    $('#services-events-pagination').bootpag({
        total: 10
    }).on("page", function (event, num) {
        $('#services-events-content div').hide();
        var current_page = '#page-' + num;
        $(current_page).show();
    });

    $('[data-lp]').addClass('page-item');
    $('.page-item > a').addClass('page-link');

    $('#services-prints-table').DataTable();

    const people = ['John Appleseed', 'Joe Rogan', 'Adam Smith', 'Mark Spencer'];

    function source(choices) {
        return function (q, callback) {
            const regexp = new RegExp(q);
            const results = [];
            for (var i = 0; i < choices.length; i++) {
                const item = choices[i];
                if (regexp.test(item)) {
                    results.push(item);
                }
            }
            callback(results);
        }
    }

    $('#m-c .typeahead').typeahead({
        highlight: true,
        minLength: 5
    }, {
        name: 'people',
        source: source(people),
        limit: 5,
        async: false
    });

    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();
});